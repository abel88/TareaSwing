package Principal;

import javax.swing.*;
import java.awt.event.*;

public class MenuPrincipal extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;

    public MenuPrincipal() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        JMenuBar menubar = new JMenuBar();

        this.setJMenuBar(menubar);

        JMenu secondMenu = new JMenu("Ayuda");
        JMenu PrimerMenu = new JMenu("Registrar Datos");
        JMenu salir=new JMenu("Salir");

        menubar.add(secondMenu);
        menubar.add(PrimerMenu);
        menubar.add(salir);

        JMenuItem firstItem = new JMenuItem("Registrar Clientes");
        JMenuItem testItem = new JMenuItem("Registrar Productos");
        JMenuItem proveedor = new JMenuItem("Registrar Proveedor");
        JMenuItem exitItem = new JMenuItem("Salir");
        JMenuItem enableItem = new JMenuItem("" +"Help");
        JMenuItem salirse = new JMenuItem("" +"SALIR");
        //JMenuItem disableItem = new JMenuItem("Eliminar Producto");


        PrimerMenu.add(firstItem);
        PrimerMenu.add(testItem);
        PrimerMenu.add(proveedor);
        PrimerMenu.addSeparator();
        PrimerMenu.add(exitItem);

        secondMenu.add(enableItem);
        //secondMenu.add(disableItem);
        salir.add(salirse);



        firstItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Clientes dialog = new Clientes();
                dialog.pack();
                dialog.setVisible(true);
                //System.exit(0);
            }
        });

        testItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Producto dialog = new Producto();
                dialog.pack();
                dialog.setVisible(true);

            }
        });
        proveedor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Proveerdor dialog=new Proveerdor();
                dialog.pack();
                dialog.setVisible(true
                );

            }
        });
        enableItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JOptionPane.showMessageDialog(null,"registre datos y depues se puede eliminar actualizano las datos en la tabla");

            }
        });

        salirse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MenuPrincipal dialog = new MenuPrincipal();
                dialog.pack();
                //dialog.setVisible(true);
                System.exit(0);
            }
        });
        /*buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });*/

        /*buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });*/

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        MenuPrincipal dialog = new MenuPrincipal();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
