package Principal;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class Clientes extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtnombre;
    private JTextField txtdireccion;
    private JTextField txtapellido;
    private JTextField txtedad;
    private JTable table1;
    private JButton eliminarButton;
    private JButton modificarButton;
    private JButton guardarButton;
    private JButton mostrarDatosButton;
    private JButton saliendoButton;
    private JButton salirButton;
    private DefaultTableModel model;
    public Clientes() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        model=new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Apellidos");
        model.addColumn("Direccion");
        model.addColumn("Edad");
        table1.setModel(model);


        /*buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });*/

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        guardarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {


                String nombre=txtnombre.getText();
                String apellido=txtapellido.getText();
                String direccion=txtdireccion.getText();
                int edad=Integer.parseInt(txtedad.getText());

                model.addRow(new Object[]{nombre,apellido,direccion,edad});
                table1.setVisible(false);
                limpiar();



            }
        });
        mostrarDatosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                table1.setVisible(true);
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                //esto es para eliminar un dato en la tabla
                model.removeRow(table1.getSelectedRow());
            }
        });
        /*modificarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {


            }
        });*/
        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

            }
        });
        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {





            }
        });


    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    private void limpiar(){
        txtnombre.setText("");
        txtapellido.setText("");
        txtdireccion.setText("");
        txtedad.setText("");
    }

    public static void main(String[] args) {
        Clientes dialog = new Clientes();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
